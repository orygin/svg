# SVG
A small library wrapping lxml and svgutils. Example diagram comes from https://draw.io.

## Installation

```bash
virtualenv -p python3 /tmp/svg_env
source /tmp/svg_env/bin/activate
pip install -r https://gitlab.com/orygin/svg/-/raw/master/requirements.txt
pip install -e git+ssh://git@gitlab.com/orygin/svg.git@master#egg=svg
```

## Preview

```bash
$ python -c "import svg; svg.run_tests(\"$VIRTUAL_ENV/src/svg/examples/diagram.svg\")"
$ git -C $VIRTUAL_ENV/src/svg/ checkout examples/diagram.svg # Reset example
```

## Usage

```python
from svg import LXMLTree, SVGTree

f = '/tmp/diagram.svg'
ellipse_tag = '{http://www.w3.org/2000/svg}ellipse'

LXMLTree(f).append_id(ellipse_tag)
LXMLTree(f).edit_label("0", "10")
SVGTree(f).set_fill('0', '#CDEB8B') 
```
