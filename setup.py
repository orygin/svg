import setuptools
from distutils.core import setup
from setuptools import find_packages
from src.svg import __version__

setup(
    name='svg',
    version=__version__,
    packages=find_packages('src'),
    package_dir={'': 'src'},
    install_requires=[
        'lxml',
        'svgutils'
    ]
)
