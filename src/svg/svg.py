#!/usr/bin/env python3
import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("main")

try:
    from svg.svg_tree import SVGTree
    from svg.lxml_tree import LXMLTree
except ModuleNotFoundError: # Ran without install
    try:
        from svg_tree import SVGTree
        from lxml_tree import LXMLTree
    except ModuleNotFoundError: # Installed with pip
        from .svg_tree import SVGTree
        from .lxml_tree import LXMLTree

def run_tests(f='/tmp/diagram.svg'):
    ellipse_tag = '{http://www.w3.org/2000/svg}ellipse'

    LXMLTree(f).append_id(ellipse_tag)
    LXMLTree(f).edit_label(0, '390')

    ids = LXMLTree(f).get_ids(ellipse_tag)
    logger.info(f"There are {len(ids)} ellipses: {ids}")

    SVGTree(f).set_fill(0, '#CDEB8B') 

if __name__ == "__main__":
    run_tests()
