#!/usr/bin/env python3
"""Wrapper for lxml.etree"""

from lxml import etree
import logging
logger = logging.getLogger("LXMLTree")

class LXMLTree:
    def __init__(self, source_file: str):
        self.source_file = source_file
        self.xml = etree.parse(source_file)
        self.root = self.xml.getroot()
        self.working_element: Element = None
        
    def append_id(self, tag: str):
        """
            For a given tag, add an id attribute with an auto-incremented value
            Example tag: {http://www.w3.org/2000/svg}ellipse
        """
        elements = self.root.iter(tag)
        for i, element in enumerate(elements):
            element.set('id', f"{i}")
            logger.info(f"Edited {i} element(s)")

    def get_ids(self, tag):
        """Return all ids for a given tag"""
        return [e.get('id') for e in self.root.iter(tag) ]

    def find_id(self, id_elem):
        """Return element with given id"""
        elements = self.root.iter()
        return [ e for e in elements if e.get('id')==str(id_elem) ][0]

    def label(_, element):
        """Returns next element's child"""
        return element.getnext().getchildren()[0]

    def edit_label(self, id_elem, value: str):
        """
            For a given id, if an elem with a text child follows, edit its text value
            May raise if there is not an elem with a text child after given id
        """
        elem = self.find_id(id_elem)
        self.label(elem).text = value
        logger.info(f"Label for {id_elem} is now {value}")

    def __del__(self):
        self.xml.write(self.source_file)
