__version__ = "1"
import logging
logging.basicConfig(level=logging.DEBUG)

from .svg import run_tests
from .svg_tree import SVGTree
from .lxml_tree import LXMLTree
