#!/usr/bin/env python3
"""Wrapper for svgutils.transform"""

from svgutils import transform as sg
import logging
logger = logging.getLogger("SVGTree")

class SVGTree:
    def __init__(self, source_file: str):
        self.svg = sg.fromfile(source_file)
        self.source_file = source_file

    def set_fill(self, elem_id: str, fill_color: str):
        """For a given elem, modify the <fill> attribute"""
        el = self.svg.find_id(elem_id)
        logger.info(f"[{elem_id}] fill was {el.root.get('fill')} and is now {fill_color}")
        el.root.set('fill', fill_color)

    def __del__(self):
        self.svg.save(self.source_file)
